/* ng_filters.pl: filters for networks

   Author: Christian Knüpfer
   E-mail: christian.knuepfer@uni-jena.de
   WWW:           

   Copyright (C) 2017-2021 Christian Knüpfer

   This file is part of network-generator.

   network-generator is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   Foobar is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

:- module(ng_filters,
          [ min_multiplicity/2,
            ego_filter/2,
            identity/1
          ]).

min_multiplicity(Min, [_, M]) :- M>=Min.

ego_filter(Egos, [(N1, N2), _]) :-
    (   member(N1, Egos)
    ;   member(N2, Egos)
    ).

%!  indentity(Term)
%
%   Succeed for every term. It can be used as a post-filter for
%   edge listes, which effectively calculates the identity function.
%
%   @arg Term   is an arbitrary term.
identity(_Term).
