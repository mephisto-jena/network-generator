/* ng_generator.pl: generation of networks wrt. externally given 
                    definitions for nodes and edges

   Author: Christian Knüpfer
   E-mail: christian.knuepfer@uni-jena.de
   WWW:           

   Copyright (C) 2017-2021 Christian Knüpfer

   This file is part of network-generator.

   network-generator is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   Foobar is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

:- module(ng_generator,
          [ edge/4,
            edge_list/3,
            generate_network/4
          ]).

edge(N1, N2, Evidence, EdgeType) :-
    call(EdgeType, N1, N2, Evidence),
    node(N1), node(N2).

edge_multiplicity(N1, N2, EdgeType, DataFilters, M) :-
    edge(N1, N2, E, EdgeType),
    call(DataFilters, E),
    findall((N1, N2),
            (   edge(N1, N2, Evidence, EdgeType),
                call(DataFilters, Evidence)
            ),
            L),
    length(L, M).

edge_list(EdgeType, DataFilters, L) :-
    setof([(N1,N2), W],
          edge_multiplicity(N1, N2, EdgeType, DataFilters, W),
          L).

write_network(File, Net) :-
    open(File, write, Out),
    write(Out, ("Source", "Target", "Weight")), eol(Out),
    write_list(Out, Net),
    close(Out).

write_list(_, []) :- !.
write_list(Out, [[(P1, P2), N]|R]) :-
    write(Out, (P1, P2, N)), eol(Out),
    write_list(Out, R).

generate_network(Outfile, DataFilters, EdgeType, EdgeFilters) :-
    atomic_list_concat([Outfile, '.log'], Logfile),
    open(Logfile, write, Log),
    git_hash(GitHash, []),
    kb_version(MTimeData, CTimeKB, GitHashKB),
    write(Log, "knowledge base"), eol(Log),
    write(Log, "\tcreated on: "), write(Log, CTimeKB), eol(Log),
    write(Log, "\twith program version (git commit): "),
    write(Log, GitHashKB), eol(Log),
    write(Log, "\tcharter data from: "),
    write(Log, MTimeData), eol(Log),
    eol(Log),
    write(Log, "network generator (git commit): "),
    write(Log, GitHash), eol(Log),
    write(Log, "data filters: "), writeq(Log, DataFilters), eol(Log),
    write(Log, "edge type: "), writeq(Log, EdgeType), eol(Log),
    write(Log, "edge filters: "), writeq(Log, EdgeFilters), eol(Log),
    close(Log),
    edge_list(EdgeType, DataFilters, L),
    include(EdgeFilters, L, Net),
    write_network(Outfile, Net).

eol(Stream) :- write(Stream, "\r\n").
