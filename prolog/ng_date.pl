/* ng_date.pl: auxiliary predicates for handeling dates

   Author: Christian Knüpfer
   E-mail: christian.knuepfer@uni-jena.de
   WWW:           

   Copyright (C) 2017-2021 Christian Knüpfer

   This file is part of network-generator.

   network-generator is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   Foobar is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

:- module(ng_date,
          [ date_diff/3,
            before/2,
            within/3,
            get_year/2
          ]).

%!  date_diff(+Date1:string, +Date2:string, -YearDiff:float) is det.
%
%   Calculates difference between two dates in years.
%
%   @arg Date1      is a string containing date in ISO 8601 standard.
%   @arg Date2      is a string containing date in ISO 8601 standard.
%   @arg YearDiff   is the difference between Date1 and Date2 in years.
date_diff(Date1, Date2, YearDiff) :-
    parse_time(Date1, iso_8601, TimeStamp1),
    parse_time(Date2, iso_8601, TimeStamp2),
    Diff is TimeStamp1-TimeStamp2,
    YearDiff is Diff/(3600*24*365.25).


%!  before(+Date1:term, +Date2:term) is det.
%!  before(+Date1:string, +Date2:string) is det.
%
%   True if Date1 is before Date2.
%
%   @arg Date1  is a date either as a string in ISO 8601 format or a
%               term of the form date(Start, End, _, _), where Start
%               and End are dates given as strings in ISO 8601 format.
%   @arg Date2  is a date in the same manner as Date1.
before(date(_, End1, _, _), date(Start2, _, _, _)) :-
    parse_time(End1, iso_8601, End1TimeStamp),
    parse_time(Start2, iso_8601, Start2TimeStamp),
    End1TimeStamp<Start2TimeStamp.
before(Date1, Date2) :-
    parse_time(Date1, iso_8601, TimeStamp1),
    parse_time(Date2, iso_8601, TimeStamp2),
    TimeStamp1 < TimeStamp2.


%!  within(+Date1:term, +Date2:term, +Years:integer) is det.
%
%   True if the Date1 and Date2 are within a time period of Years
%   years.
%
%   @arg Date1  is a term of the form date(Start, End, _, _), where
%               Start and End are dates given as strings in ISO 8601
%               format.
%   @arg Date2  is a date in the same manner as Date1.
%   @arg Years  number of years.
within(date(Start1, _, _, _), date(_, End2, _, _), Years) :-
    date_diff(Start1, End2, YearDiff),
    abs(YearDiff) =< Years.


%!  get_year(+IsoDate:string, -Year:float).
%
%   Returns the year of the given date.
%
%   @arg IsoDate is a date given as a string in ISO 8601 format.
%   @arg Years number of years.
get_year(IsoDate, Year) :-
    parse_time(IsoDate, iso_8601, TimeStamp),
    stamp_date_time(TimeStamp, Date, 'UTC'),
    date_time_value('year', Date, Year).
