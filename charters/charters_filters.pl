/* charters_filters.pl: filters for networks based on charters

   Author: Christian Knüpfer
   E-mail: christian.knuepfer@uni-jena.de
   WWW:           

   Copyright (C) 2017-2021 Christian Knüpfer

   This file is part of network-generator.

   network-generator is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   Foobar is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

:- module(charters_filters,
          [ filter_charters/2,
            filter_charters_by_IDs/2,
            filter_charters_issuers/2,
            filter_charters_timerange/3
          ]).

:- prolog_load_context(directory, Dir),
   absolute_file_name('../prolog/ng_date', File, [relative_to(Dir)]),
   use_module(File).

filter_charters(Filter, C) :-
    \+ is_list(Filter),
    call(Filter, C).
filter_charters([], C) :-
    charter(C, _, _, _).
filter_charters([Filter|Filters], C) :-
    call(Filter, C),
    filter_charters(Filters, C).

filter_charters_by_IDs(IDs, C) :-
    charter(C, _, _, _),
    member(C, IDs).

filter_charters_issuers(all, _).
filter_charters_issuers(Issuers, C) :-
    is_list(Issuers),
    charter(C, _, _, _),
    member(Issuer, Issuers)
    issuer(Issuer, C).

filter_charters_timerange(From, To, C) :-
    charter(C, _, Date, _),
    in_timerange(Date, From, To).

in_timerange(date(Start, End, _, _), FromYear, ToYear) :-
    get_year(Start, StartYear),
    get_year(End, EndYear),
    FromYear=<EndYear,
    FromYear=<StartYear,
    EndYear=<ToYear.
