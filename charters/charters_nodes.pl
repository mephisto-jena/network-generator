/* charters_nodes.pl: node definitions based on charters

   Author: Christian Knüpfer
   E-mail: christian.knuepfer@uni-jena.de
   WWW:           

   Copyright (C) 2017-2021 Christian Knüpfer

   This file is part of network-generator.

   network-generator is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   Foobar is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

:- module(charters_nodes,
          [ preceding/2,
            find_duplicate_persons/0,
            node/1
          ]).

% - nodes are involved persons
node(P) :- person(P, _, _, _, _).

                 /*******************************
                 *            SORTING           *
                 *******************************/

superior('Kaiser', 'Kaiserin').
superior('Kaiserin', 'Erzbischof').
superior('Erzbischof', 'Bischof').
superior('Bischof', 'Abt').
superior('Abt', 'Herzog').
superior('Herzog', 'Landgraf').
superior('Landgraf', 'Markgraf').
superior('Markgraf', 'Pfalzgraf').
superior('Pfalzgraf', 'Graf').
superior('Graf', 'Vizegraf').
superior('Vizegraf', 'Vogt').
superior('Vogt', 'Sonstige').

superiority(P1, P2) :- superior(P1, P2).
superiority(P1, P2) :-
    superior(P1, P),
    superiority(P, P2).

rank(P1, P2) :-
    person(P1, _, _, R1, N1),
    person(P2, _, _, R2, N2),
    R1==R2,
    N1<N2.
rank(P1, P2) :-
    person(P1, _, _, R1, _),
    person(P2, _, _, R2, _),
    R1\==R2,
    superiority(R1, R2).

preceding(P1, P2) :-
    person(P1, _, _, _, N1),
    person(P2, _, _, _, N2),
    N1<N2.


                 /*******************************
                 *       CONSISTENCY CHECKS     *
                 *******************************/

find_duplicate_persons :-
    foreach(persons_abbr_eq(P, T1, N1, T2, N2),
            (write("WARNING: ambiguous person symbol \'"),
             write(P), write("\'"), nl, write("\t"),
             write(T1), write(", "), write(N1),
             write(" <--> "),
             write(T2), write(", "), write(N2),
             nl)).

persons_abbr_eq(P, T1, N1, T2, N2) :-
    person(P, T1, N1, _, _),
    person(P, T2, N2, _, _),
    T1\==T2, T1@<T2.
persons_abbr_eq(P, T1, N1, T2, N2) :-
    person(P, T1, N1, _, _),
    person(P, T2, N2, _, _),
    T1==T2, N1\==N2, N1@<N2.

