/* charters_evidences.pl: calculate evidences for network edges

   Author: Christian Knüpfer
   E-mail: christian.knuepfer@uni-jena.de
   WWW:           

   Copyright (C) 2017-2021 Christian Knüpfer

   This file is part of network-generator.

   network-generator is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   Foobar is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

:- module(charters_evidences,
          [ list_charter_evidences/3 ]).
              
list_charter_evidences(N1, N2, Pred) :-
    findall((StartDate, C), (
                call(Pred, N1, N2, C),
                charter(C, _, date(StartDate, _, _, _), _)
            ), L),
    sort(L, LSorted),
    forall(member((_, C), LSorted), write_charter(C)).

write_charter(C) :-
    charter(C, _, date(Start, End, _, _), _),
    write_term(C,[quoted(true)]),
    write(" ("),
    write(Start),
    write(" -- "),
    write(End),
    writeln(")").
