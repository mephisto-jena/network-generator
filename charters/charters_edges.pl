/* charters_edges.pl: edge definitions based on charters

   Author: Christian Knüpfer
   E-mail: christian.knuepfer@uni-jena.de
   WWW:           

   Copyright (C) 2017-2021 Christian Knüpfer

   This file is part of network-generator.

   network-generator is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   Foobar is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

:- module(charter_edges,
          [ co_testimony/4,
            loyalty/3,
            solidarity/3,
            mediation/3,
            grant/3,
            endorsement/3,
            co_certification/4,
            co_certification_yearly/4
          ]).

:- prolog_load_context(directory, Dir),
   absolute_file_name('../prolog/ng_date', File, [relative_to(Dir)]),
   use_module(File).

co_testimony(Order, Person1, Person2, Charter) :-
    witness(Person1, Charter),
    witness(Person2, Charter), % make undirected edges
    call(Order, Person1, Person2).
                
loyalty(Person, Issuer, Charter) :-
    issuer(Issuer, Charter),
    witness(Person, Charter).
                
solidarity(Person, Recipient, Charter) :-
    recipient(Recipient, Charter),
    witness(Person, Charter).
                
mediation(Intervener, Person, Charter) :-
    intervener(Intervener, Charter),
    (   recipient(Person, Charter)
    ;   issuer(Person, Charter)
    ).
                
grant(Issuer, Recipient, Charter) :-
    issuer(Issuer, Charter),
    recipient(Recipient, Charter).
                
endorsement(Person1, Person2, Charter) :-
    (   grant(Person2, Person1, Charter) % direction reversed!
    ;   loyalty(Person1, Person2, Charter)
    ;   solidarity(Person1, Person2, Charter)
    ).
                
co_certification(Order, Person1, Person2, Charter) :-
    certification_participant(Person1, Charter),
    certification_participant(Person2, Charter),
    call(Order, Person1, Person2).
                
co_certification_yearly(Order, Person1, Person2, Charter1) :-
    co_certification(Order, Person1, Person2, Charter1),
    charter(Charter1, _, date(Date1, _, _, _), ID1),
    get_year(Date1, Year1),
    findall(_,
            (   co_certification(Order, Person1, Person2, Charter2),
                Charter1\==Charter2,
                charter(Charter2, _, date(Date2, _, _, _), ID2),
                get_year(Date2, Year2),
                Year1=Year2,
                (   before(Date2, Date1)
                ;   Date1==Date2, ID2<ID1
                )
            ), L),
    length(L, 0).
                
certification_participant(Person, Charter) :-
    (   issuer(Person, Charter)
    ;   recipient(Person, Charter)
    ;   intervener(Person, Charter)
    ;   witness(Person, Charter)
    ).                
