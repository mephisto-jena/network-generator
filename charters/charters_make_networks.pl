/* charters_make_network.pl: generation of networks based on charters

   Author: Christian Knüpfer
   E-mail: christian.knuepfer@uni-jena.de
   WWW:           

   Copyright (C) 2017-2021 Christian Knüpfer

   This file is part of network-generator.

   network-generator is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.

   Foobar is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

:- module(charters_make_networks,
          [ make_network/6,
            make_networks/3
          ]).

:- prolog_load_context(directory, Dir),
   absolute_file_name('../prolog/ng_generator', GenFile, [relative_to(Dir)]),
   absolute_file_name('../prolog/ng_filters', FilterFile, [relative_to(Dir)]),
   reexport([GenFile,
             FilterFile]). % load network generator and filters

:- reexport(['charters_nodes.pl',
             'charters_filters.pl',
             'charters_edges.pl']). % load network definitions

issuers_string(all, "_all") :- !.
issuers_string(Issuers, IStr) :-
    is_list(Issuers),
    findall(S, (member(X, Issuers), re_replace("/", "", X, S)), L),
    atomic_list_concat(L, "+", LS),
    atomic_list_concat(["_", LS], IStr).

min_string(Min, "") :- Min =<1, !.
min_string(Min, MStr) :-
    Min >1,
    atomic_list_concat(["_min", Min], MStr).

make_network(EdgeType, From, To, Issuers, Min, OutDir) :-
    issuers_string(Issuers, IStr),
    min_string(Min, MStr),
    term_to_atom(EdgeType, EdgeTypeAtom),
    term_to_atom(From, FromAtom),
    term_to_atom(To, ToAtom),
    atomic_list_concat([OutDir, EdgeTypeAtom, "_",
                        FromAtom, "-", ToAtom, IStr, MStr,
                        ".csv"], OutfileName),
    absolute_file_name(OutfileName, Outfile, [expand(true)]),
    write("generate network "), write(Outfile), writeln("..."),
    generate_network(Outfile,
                     filter_charters(
                         [   filter_charters_issuers(Issuers),
                             filter_charters_timerange(From, To)
                         ]),
                     EdgeType,
                     min_multiplicity(Min)).

make_networks(EdgeType, TaskList, OutDir) :-
    forall(member((From, To, Issuers, Min), TaskList),
           make_network(EdgeType, From, To, Issuers, Min, OutDir)).
