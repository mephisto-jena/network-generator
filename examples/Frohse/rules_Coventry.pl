participate(meeting_Frohse, P) :-
    person(P, _, _, _),
    mentions(chroniconIV-52, P).

node(P) :- participate(meeting_Frohse, P).

co_participation(P1, P2) :-
    participate(meeting_Frohse, P1),
    participate(meeting_Frohse, P2).
