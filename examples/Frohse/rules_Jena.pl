:- module(rules_Jena,
          [ node/1,
            positive_edge/3,
            negative_edge/2
          ]).

participate(meeting_Frohse, P) :-
    person(P, _, _, _),
    (
        mentions(chroniconIV-52, P)
    ;   mentions(chroniconIV-52, suffragan_bishop(giselher)),
        suffragan_bishop(P, giselher)
    ).

node(P) :- person(P, _, _, _),
           participate(meeting_Frohse, P).
node(P) :- person(P, _, _, _),
           mentions(chroniconIV-52, their_lord),
           their_lord(P).

positive_edge(_, P1, P2) :-
    (
        relatives(P1, P2)
    ;   subordination(P1, P2)
    ;   mentions(chroniconIV-52, support(P1, P2))
    ).
        
negative_edge(P1, P2) :-
    mentions(chroniconIV-52, conflict(P1, P2)).
