kb_version("", "", "").

person(otto, "Otto III", "Holy Roman Emperor", "https://d-nb.info/gnd/118590766").
person(bernhard, "Bernhard I", "Duke of Saxony", "https://d-nb.info/gnd/137529511").
person(liuthar, "Liuthar III of Walbeck‏", "Margrave of the Nordmark", "https://d-nb.info/gnd/137954409").
person(ekkehard, "Ekkehard I", "Margrave of Meissen", "https://d-nb.info/gnd/123899168").
person(gero, "Gero II", "Margrave of Lusatia", "https://d-nb.info/gnd/136648983").
person(gunzelin, "Gunzelin of Kuckenburg", "Count of Frohse", "https://d-nb.info/gnd/137532121").
person(giselher, "Giselher", "Archbishop of Magdeburg", "https://d-nb.info/gnd/136659330").
person(hugo, "Hugo II", "Bishop of Zeitz", "http://d-nb.info/gnd/1170093493").
person(eido, "Eido I", "Bishop of Meissen", "https://d-nb.info/gnd/135901278").

mentions(chroniconIV-52, their_lord).
mentions(chroniconIV-52, gunzelin).
mentions(chroniconIV-52, giselher).
mentions(chroniconIV-52, suffragan_bishop(giselher)).
mentions(chroniconIV-52, bernhard).
mentions(chroniconIV-52, liuthar).
mentions(chroniconIV-52, ekkehard).
mentions(chroniconIV-52, gero).
mentions(chroniconIV-52, support(liuthar, giselher)).
mentions(chroniconIV-52, conflict(liuthar, ekkehard)).

their_lord(otto).
suffragan_bishop(hugo, giselher).
suffragan_bishop(eido, giselher).

brother(gunzelin, ekkehard).
relatives(P1, P2) :- brother(P1, P2).

margrave(liuthar, bernhard).
margrave(ekkehard, bernhard).
margrave(gero, bernhard).

fief(frohse, otto, gero).

subordination(P1, P2) :-
    (
        suffragan_bishop(P1, P2)
    ;   margrave(P1, P2)
    ;   fief(_, P2, P1)
    ).
